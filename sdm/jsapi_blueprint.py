from flask import Blueprint
from flask import jsonify

from sdm.settings import TEMPLATE_DIR

jsapi_blueprint = Blueprint("jsapi_blueprint", __name__, template_folder=TEMPLATE_DIR)  # noqa: E501

jsapi_root = "js-api"
jsapi_version = "1.0"
jsapi_spec_version = "0.1"
jsapi_status = "good"
jsapi_test_dic = {"status": jsapi_status}
jsapi_info_dic = {"status": jsapi_status, "version": jsapi_version, "system": jsapi_root}


@jsapi_blueprint.route(f"/{jsapi_root}/v" + jsapi_version + "/post/rpc", methods=["POST"])
def jsapi_rpc():
    return jsonify("no")


@jsapi_blueprint.route(f"/{jsapi_root}/system/health")
def jsapi_health():
    return jsonify({"status": "healthy"})


@jsapi_blueprint.route(f"/{jsapi_root}/system/test")
def jsapi_test():
    return jsonify(jsapi_test_dic)


@jsapi_blueprint.route(f"/{jsapi_root}/system/info")
def jsapi_info():
    return jsonify(jsapi_info_dic)
