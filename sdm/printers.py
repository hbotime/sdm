from contracts import contract
from texttable import Texttable

from sdm.settings import VERSION as versionnumber, DB_URL, TEXTTABLE_STYLE

from sdm.core import (
    test_pgdb_conn,
    test_mgdb_conn,
    store_imported_file_from_path,
    compile_list_of_uploaded_files,
)

@contract(returns=None)
def print_version():
    print(versionnumber)


@contract(returns=None)
def print_kv_pairs(dic):
    assert isinstance(dic, dict) is True
    for key in dic.keys():
        print(f"{key} =", dic[key])


@contract(returns=None)
def print_showdbconnstring():
    print(DB_URL)


@contract(returns=None)
def print_test_pgdb_conn():
    print(test_pgdb_conn())


@contract(returns=None)
def print_test_mgdb_conn():
    print(test_mgdb_conn())


@contract(returns=None)
def print_list_of_uploaded_files():
    list_of_uploaded_files = compile_list_of_uploaded_files()
    t = Texttable()
    t.set_chars(TEXTTABLE_STYLE)
    t.add_row(["NAME", "SIZE", "HASH"])
    for uploadfile in list_of_uploaded_files:
        t.add_row([uploadfile, "Not Implemented", "Not Implemented"])
    print(t.draw())
