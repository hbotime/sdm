function create_menu(basepath)
{
	var base = (basepath == 'null') ? '' : basepath;

	document.write(
		'<table cellpadding="0" cellspaceing="0" border="0" style="width:98%"><tr>' +
		'<td class="td" valign="top">' +

		'<ul>' +
		'<li><a href="'+base+'/">SDM Home</a></li>' +
		'</ul>' +

		'<h3>Basic Info</h3>' +
		'<ul>' +
			'<li><a href="'+base+'/top100/article/requirements">Server Requirements</a></li>' +
			'<li><a href="'+base+'/top100/article/license">License Agreement</a></li>' +
			'<li><a href="'+base+'/top100/article/changelog">Change Log</a></li>' +
			'<li><a href="'+base+'/top100/article/general/credits">Credits</a></li>' +
		'</ul>' +

		'<h3>Installation</h3>' +
		'<ul>' +
			'<li><a href="'+base+'/top100/article/downloads">Downloading CodeIgniter</a></li>' +
			'<li><a href="'+base+'/top100/article/index">Installation Instructions</a></li>' +
			'<li><a href="'+base+'/top100/article/upgrading">Upgrading from a Previous Version</a></li>' +
			'<li><a href="'+base+'/top100/article/troubleshooting">Troubleshooting</a></li>' +
		'</ul>' +

		'<h3>Introduction</h3>' +
		'<ul>' +
			'<li><a href="'+base+'/top100/article/getting_started">Getting Started</a></li>' +
			'<li><a href="'+base+'/top100/article/at_a_glance">CodeIgniter at a Glance</a></li>' +
			'<li><a href="'+base+'/top100/article/cheatsheets">CodeIgniter Cheatsheets</a></li>' +
			'<li><a href="'+base+'/top100/article/features">Supported Features</a></li>' +
			'<li><a href="'+base+'/top100/article/appflow">Application Flow Chart</a></li>' +
			'<li><a href="'+base+'/top100/article/mvc">Model-View-Controller</a></li>' +
			'<li><a href="'+base+'/top100/article/goals">Architectural Goals</a></li>' +
		'</ul>' +

		'<h3>Tutorial</h3>' +
		'<ul>' +
			'<li><a href="'+base+'/top100/article/index">Introduction</a></li>' +
			'<li><a href="'+base+'/top100/article/static_pages">Static pages</a></li>' +
			'<li><a href="'+base+'/top100/article/news_section">News section</a></li>' +
			'<li><a href="'+base+'/top100/article/create_news_items">Create news items</a></li>' +
			'<li><a href="'+base+'/top100/article/conclusion">Conclusion</a></li>' +
		'</ul>' +

		'</td><td class="td_sep" valign="top">' +

		'<h3>General Topics</h3>' +
		'<ul>' +
			'<li><a href="'+base+'/top100/article/urls">CodeIgniter URLs</a></li>' +
			'<li><a href="'+base+'/top100/article/controllers">Controllers</a></li>' +
			'<li><a href="'+base+'/top100/article/reserved_names">Reserved Names</a></li>' +
			'<li><a href="'+base+'/top100/article/views">Views</a></li>' +
			'<li><a href="'+base+'/top100/article/models">Models</a></li>' +
			'<li><a href="'+base+'/top100/article/helpers">Helpers</a></li>' +
			'<li><a href="'+base+'/top100/article/libraries">Using CodeIgniter Libraries</a></li>' +
			'<li><a href="'+base+'/top100/article/creating_libraries">Creating Your Own Libraries</a></li>' +
			'<li><a href="'+base+'/top100/article/drivers">Using CodeIgniter Drivers</a></li>' +
			'<li><a href="'+base+'/top100/article/creating_drivers">Creating Your Own Drivers</a></li>' +
			'<li><a href="'+base+'/top100/article/core_classes">Creating Core Classes</a></li>' +
			'<li><a href="'+base+'/top100/article/hooks">Hooks - Extending the Core</a></li>' +
			'<li><a href="'+base+'/top100/article/autoloader">Auto-loading Resources</a></li>' +
			'<li><a href="'+base+'/top100/article/common_functions">Common Functions</a></li>' +
			'<li><a href="'+base+'/top100/article/routing">URI Routing</a></li>' +
			'<li><a href="'+base+'/top100/article/errors">Error Handling</a></li>' +
			'<li><a href="'+base+'/top100/article/caching">Caching</a></li>' +
			'<li><a href="'+base+'/top100/article/profiling">Profiling Your Application</a></li>' +
			'<li><a href="'+base+'/top100/article/cli">Running via the CLI</a></li>' +
			'<li><a href="'+base+'/top100/article/managing_apps">Managing Applications</a></li>' +
			'<li><a href="'+base+'/top100/article/environments">Handling Multiple Environments</a></li>' +
			'<li><a href="'+base+'/top100/article/alternative_php">Alternative PHP Syntax</a></li>' +
			'<li><a href="'+base+'/top100/article/security">Security</a></li>' +
			'<li><a href="'+base+'/top100/article/styleguide">PHP Style Guide</a></li>' +
			'<li><a href="'+base+'doc_style/index">Writing Documentation</a></li>' +
		'</ul>' +

		'</td><td class="td_sep" valign="top">' +

		'<h3>Class Reference</h3>' +
		'<ul>' +
		'<li><a href="'+base+'/top100/article/benchmark">Benchmarking Class</a></li>' +
		'<li><a href="'+base+'/top100/article/calendar">Calendar Class</a></li>' +
		'<li><a href="'+base+'/top100/article/cart">Cart Class</a></li>' +
		'<li><a href="'+base+'/top100/article/config">Config Class</a></li>' +
		'<li><a href="'+base+'/top100/article/email">Email Class</a></li>' +
		'<li><a href="'+base+'/top100/article/encryption">Encryption Class</a></li>' +
		'<li><a href="'+base+'/top100/article/file_uploading">File Uploading Class</a></li>' +
		'<li><a href="'+base+'/top100/article/form_validation">Form Validation Class</a></li>' +
		'<li><a href="'+base+'/top100/article/ftp">FTP Class</a></li>' +
		'<li><a href="'+base+'/top100/article/table">HTML Table Class</a></li>' +
		'<li><a href="'+base+'/top100/article/image_lib">Image Manipulation Class</a></li>' +
		'<li><a href="'+base+'/top100/article/input">Input Class</a></li>' +
		'<li><a href="'+base+'/top100/article/javascript">Javascript Class</a></li>' +
		'<li><a href="'+base+'/top100/article/loader">Loader Class</a></li>' +
		'<li><a href="'+base+'/top100/article/language">Language Class</a></li>' +
		'<li><a href="'+base+'/top100/article/migration">Migration Class</a></li>' +
		'<li><a href="'+base+'/top100/article/output">Output Class</a></li>' +
		'<li><a href="'+base+'/top100/article/pagination">Pagination Class</a></li>' +
		'<li><a href="'+base+'/top100/article/security">Security Class</a></li>' +
		'<li><a href="'+base+'/top100/article/sessions">Session Class</a></li>' +
		'<li><a href="'+base+'/top100/article/trackback">Trackback Class</a></li>' +
		'<li><a href="'+base+'/top100/article/parser">Template Parser Class</a></li>' +
		'<li><a href="'+base+'/top100/article/typography">Typography Class</a></li>' +
		'<li><a href="'+base+'/top100/article/unit_testing">Unit Testing Class</a></li>' +
		'<li><a href="'+base+'/top100/article/uri">URI Class</a></li>' +
		'<li><a href="'+base+'/top100/article/user_agent">User Agent Class</a></li>' +
		'<li><a href="'+base+'/top100/article/xmlrpc">XML-RPC Class</a></li>' +
		'<li><a href="'+base+'/top100/article/zip">Zip Encoding Class</a></li>' +
		'</ul>' +

		'</td><td class="td_sep" valign="top">' +

		'<h3>Driver Reference</h3>' +
		'<ul>' +
		'<li><a href="'+base+'/top100/article/caching">Caching Class</a></li>' +
		'<li><a href="'+base+'/top100/article/index">Database Class</a></li>' +
		'<li><a href="'+base+'/top100/article/javascript">Javascript Class</a></li>' +
		'</ul>' +

		'<h3>Helper Reference</h3>' +
		'<ul>' +
		'<li><a href="'+base+'/top100/article/array_helper">Array Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/captcha_helper">CAPTCHA Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/cookie_helper">Cookie Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/date_helper">Date Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/directory_helper">Directory Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/download_helper">Download Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/email_helper">Email Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/file_helper">File Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/form_helper">Form Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/html_helper">HTML Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/inflector_helper">Inflector Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/language_helper">Language Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/number_helper">Number Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/path_helper">Path Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/security_helper">Security Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/smiley_helper">Smiley Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/string_helper">String Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/text_helper">Text Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/typography_helper">Typography Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/url_helper">URL Helper</a></li>' +
		'<li><a href="'+base+'/top100/article/xml_helper">XML Helper</a></li>' +
		'</ul>' +

		'</td></tr></table>');
}
