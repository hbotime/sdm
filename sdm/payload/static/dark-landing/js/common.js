	var supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
	var winScr;

	$(window).scroll(function(){
		winScr = $(window).scrollTop();
		parallaxFunction();
	});

	$(document).ready(function(){
		winScr = $(window).scrollTop();
		parallaxFunction();
	});

	$(window).load(function(){
		winScr = $(window).scrollTop();
		parallaxFunction();
		centeredTitles();

		//Menu vertical align
		$('#cssmenu > ul').css("margin-top", ($('#cssmenu > .logo').height() / 2)-14);
	});

	$(window).on('resize orientationchange', function(){
		winScr = $(window).scrollTop();
		parallaxFunction();
		resizeFix();
	});

/*********************************************************************************************************/
/* -------------------------------------- BLOG CENTERED TITLE ------------------------------------------ */
/*********************************************************************************************************/
function centeredTitles(){
	$(".post-data-single").each(function(){
		$(this).find(".date-singlep").css("max-height", $(this).find(".all-single-prop").height());
		$(this).find(".date-singlep").height($(this).outerHeight()-44);
	});

	$(".post-data").each(function(){
		$(this).find(".post-arch-title").height($(this).height());
		$(this).find(".date-posted").css("max-height", $(this).find(".post-arch-title").height());
		$(this).find(".date-posted").height(($(this).outerHeight())/2+30);
	});
};


/******************************************************************************************************/
/* -------------------------------------- PARALLAX FUNCTION ----------------------------------------- */
/******************************************************************************************************/
	function parallaxFunction(){
		var h = $(window).height();
		if(winScr < $("#home").outerHeight()){
			$(".parallax-bg").css({top : (winScr/0.9)+0});
			$(".parallax-overlay").css({top : (h/2.5)+(winScr/2.5)});
			$(".parallax-bg-overlay").css({top: -(h/4.5)+(winScr/1.2)});
			$(".homeoverlay").css({top : (h/2)-50});
		}
	};
