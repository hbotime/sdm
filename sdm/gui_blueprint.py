from flask import Blueprint
from flask import render_template, jsonify, make_response
from flask import send_from_directory
from flask_security.core import current_user
from bs4 import BeautifulSoup

from sdm.settings import TEMPLATE_DIR, PAYLOADPATH

from sdm.system_info import get_box_specs
from sdm.cache import cache

gui_blueprint = Blueprint("gui_blueprint", __name__, template_folder=TEMPLATE_DIR)  # noqa: E501

gui_root = "gui"
gui_version = "1.0"
gui_spec_version = "0.1"
gui_status = "good"
gui_test_dic = {"status": gui_status}
gui_info_dic = {"status": gui_status, "version": gui_version, "system": gui_root}


@gui_blueprint.route("/")
def gui_index():
    tree = BeautifulSoup(render_template("root.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press")
def gui_press():
    tree = BeautifulSoup(render_template("press-root.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press/team")
def gui_press_team():
    tree = BeautifulSoup(render_template("press-team.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press/partners")
def gui_press_partners():
    tree = BeautifulSoup(render_template("press-partners.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press/terms")
def gui_press_terms():
    return render_template("press-terms.html")


@gui_blueprint.route("/press/knowledgebase")
def gui_press_knowledgebase():
    tree = BeautifulSoup(render_template("press-knowledgebase.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press/knowledgebase/article")
def gui_press_knowledgebase_article():
    tree = BeautifulSoup(render_template("press-knowledgebase-article.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press/knowledgebase/category")
def gui_press_knowledgebase_category():
    tree = BeautifulSoup(render_template("press-knowledgebase-category.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press/knowledgebase/components")
def gui_press_knowledgebase_components():
    tree = BeautifulSoup(render_template("press-knowledgebase-components.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press/knowledgebase/contact")
def gui_press_knowledgebase_contact():
    tree = BeautifulSoup(render_template("press-knowledgebase-contact.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press/knowledgebase/faq")
def gui_press_knowledgebase_faq():
    tree = BeautifulSoup(render_template("press-knowledgebase-faq.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/press/knowledgebase/article-narrow")
def gui_press_knowledgebase_articlenarrow():
    tree = BeautifulSoup(render_template("press-knowledgebase-narrow.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/admin")
# @login_required
def gui_admin():
    tree = BeautifulSoup(render_template("admin-root.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/admin/knowledgebase")
# @login_required
def gui_admin_knowledgebase():
    tree = BeautifulSoup(render_template("admin-knowledgebase.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/admin/signup")
def gui_admin_signup():
    return render_template("admin-signup.html")


@gui_blueprint.route("/admin/404")
# @login_required
def gui_admin_err_404():
    return render_template("admin-404.html")


@gui_blueprint.route("/admin/sessionexpired")
# @login_required
def gui_admin_sessionexpired():
    return render_template("admin-sessionexpired.html")


@gui_blueprint.route("/top100")
def gui_top100():
    tree = BeautifulSoup(render_template("top100.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/top100/toc")
def gui_top100_toc():
    tree = BeautifulSoup(render_template("top100-toc.html"))
    good_html = tree.prettify()
    return good_html


@gui_blueprint.route("/top100/article/<filename>")
def gui_top100_article(filename):
    return render_template("topics/" + filename + ".html")


@gui_blueprint.route("/techarticles/<filename>")
def gui_techarticles(filename):
    #return send_from_directory("/persistentworkingdirs/techdocs/_build/html", "index.html")
    return render_template("tech/test.html")


@gui_blueprint.route("/matrix")
def gui_sdm():
    return send_from_directory(PAYLOADPATH + "/templates/matrix", "index.html")


@gui_blueprint.route("/matrix/<filename>")
def gui_matrix_misc(filename):
    return send_from_directory(PAYLOADPATH + "/templates/matrix", filename)


@gui_blueprint.route("/static/<filename>")
@gui_blueprint.route("/static/<path:path>/<filename>")
def gui_custom_static(filename, path=None):
    if not path:
        path = ""
    return send_from_directory(PAYLOADPATH + "/static/" + path, filename)


@gui_blueprint.route("/techarticles/_static/<filename>")
def gui_custom_static_for_tech_articles(filename, path=None):
    return send_from_directory(PAYLOADPATH + "/static/tech", filename)


@gui_blueprint.route("/lib/<filename>")
def gui_custom_lib(filename):
    return send_from_directory("/vagrant/files/lib/consolidated", filename + ".html")


@gui_blueprint.route("/lib/<path:path>/<filename>")
def gui_custom_static_for__lib(path, filename):
    print("/vagrant/files/lib/consolidated" + "/" + path)
    return send_from_directory("/vagrant/files/lib/consolidated" + "/" + path, filename)


@gui_blueprint.route("/test")
def gui_test():
    return render_template("system-test.html")


@gui_blueprint.route("/boxinfo")
def gui_boxinfo():
    return get_box_specs()


@gui_blueprint.route("/system/health")
def gui_system_health():
    return jsonify({"status": "healthy"})


@gui_blueprint.route("/system/test")
def gui_system_test():
    return jsonify(gui_test_dic)


@gui_blueprint.route("/system/info")
def gui_system_info():
    return jsonify(gui_info_dic)
