import os
import datetime

from multiprocessing import Process

from pymongo import MongoClient
from humbledb import Mongo
from bson.objectid import ObjectId
from sqlalchemy import create_engine
from contracts import contract
from dotty_dict import dotty

from sdm.utils import (
    list_file_paths,
    file_read,
    file_write,
    file_delete,
    b64encode,
    dir_exists,
    dir_create,
    file_read_json,
    random_tag,
    list_files,
)  # noqa: E501
from sdm.settings import DB_URL


def test_pgdb_conn():
    raw_engine = create_engine(DB_URL)
    raw_conn = raw_engine.connect()
    raw_conn.execute(
        """CREATE TABLE IF NOT EXISTS films (title text,
        director text,
        year text)"""
    )
    result = raw_conn.execute(
        """select column_name,
        data_type from information_schema.columns where table_name = 'films'"""
    )
    rows = result.fetchall()
    raw_conn.close()

    s = ""
    for row in rows:
        s = s + str(row) + "\n"
    return s


def test_mgdb_conn():
    client = MongoClient("localhost", 27017)
    db = client["test-database"]
    collection = db["test-collection"]
    articles = collection.articles
    # articles.remove({"author": "Mike"})
    if articles.find_one({"author": "Mike"}) is None:
        post = {"author": "Mike", "text": "My first blog post!", "tags": ["mongodb", "python", "pymongo"], "date": datetime.datetime.utcnow()}
        articles.insert_one(post)

    for article in articles.find():
        print(article)
    return "result.inserted_id"


def getListOfFiles(dirName):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)

    return allFiles

def taxonamy():
    path ="/persistentworkingdirs/articles"
    lst = getListOfFiles(path)
    dot = dotty()
    for _ in lst:
        fpath = _
        _ = _[1:]
        _ = _.replace('.','-')
        _ = _.replace('/','.')
        dot[_] = file_read(fpath).decode("utf-8") 
    return dot["persistentworkingdirs.articles"]


def store_imported_file_from_path(filepath):
    filedata = file_read(filepath)
    filedata = filedata.decode("utf-8")
    return store_imported_file_from_string(filedata)


def store_imported_file_from_string(filedata):
    uploads_dir = "/tmp/sdm/uploads"
    dir_create(os.path.abspath(uploads_dir))
    tag = random_tag()
    filepath = os.path.abspath(os.path.join(uploads_dir, tag))
    file_write(filepath, filedata)
    return {"tag": tag, "length": len(filedata)}


def delete_uploaded_file(filename):
    path = "/tmp/sdm/uploads"
    filepath = os.path.abspath(os.path.join(path, filename))
    file_delete(filepath)


def delete_all_uploaded_files():
    filelist = compile_list_of_uploaded_files()
    for f in filelist:
        delete_uploaded_file(f)


def compile_list_of_uploaded_files():
    path = "/tmp/sdm/uploads"
    dir_create(path)
    return list_files(path)
