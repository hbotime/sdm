import os
import shutil
from pathlib import Path
from shutil import copyfile, move, rmtree
from subprocess import Popen, PIPE

import pytest
import click

from sdm.settings import server_port, socket_host, CONFIG_DIC, BASEDIR, PAYLOADPATH

from sdm.cherrypy_app import cherrypy as cherrypy_app
from sdm.flask_app import app as flask_app
from sdm.core import taxonamy

from sdm.utils import (
    list_file_paths,
    file_read,
    file_write,
    file_delete,
    file_copy,
    b64encode,
    dir_exists,
    dir_create,
    dir_delete,
    file_read_json,
    random_tag,
    list_files,
)  # noqa: E501

@click.group()
def cli():
    return None


@click.group()
def servers():
    return None


@click.group()
def fs():
    return None


@click.group()
def docs():
    return None


@servers.command()
def runcherrypyserver():
    cherrypy_app.engine.start()


@servers.command()
def runflaskserver():
    flask_app.run(host=socket_host, port=server_port)


@fs.command()
def listtaxonamy():
    print(taxonamy())


@docs.command()
def render():
    from bash import bash
    os.chdir('/vagrant/files/docs/30150eee-5c07-11ea-bc55-0242ac130003/raw')
    print(bash('make html'))
    os.chdir('/vagrant/files/docs/30150eee-5c07-11ea-bc55-0242ac130003')
    dir_delete('/vagrant/files/docs/30150eee-5c07-11ea-bc55-0242ac130003/rendered')
    dir_create('/vagrant/files/docs/30150eee-5c07-11ea-bc55-0242ac130003/rendered')
    file_copy('/vagrant/files/docs/30150eee-5c07-11ea-bc55-0242ac130003/raw/_build/html/index.html', '/vagrant/files/docs/30150eee-5c07-11ea-bc55-0242ac130003/rendered/index.html')
    shutil.copytree('/vagrant/files/docs/30150eee-5c07-11ea-bc55-0242ac130003/raw/_build/html/_static', '/vagrant/files/docs/30150eee-5c07-11ea-bc55-0242ac130003/rendered/_static')


@click.group()
def testing():
    return None


@testing.command()
def selftest():
    pytest.main(["-x", "-v", BASEDIR])


@testing.command()
def selfcoverage():
    pytest.main(["--cov", BASEDIR, "--cov-report", "term-missing"])

cli.add_command(fs)
cli.add_command(docs)
cli.add_command(servers)
cli.add_command(testing)
main = cli
