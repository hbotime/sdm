import sys
from setuptools import setup, find_packages

from sdm.settings import VERSION, MINIMUM_PYTHON_VERSION, reponame

assert sys.version_info >= MINIMUM_PYTHON_VERSION

setup(
    name=reponame,
    version=VERSION,
    description="sdm",
    author="Terminal Labs",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=[
        "setuptools",
        "utilities-package@git+https://gitlab.com/terminallabs/utilitiespackage/utilities-package.git@master#egg=utilitiespackage&subdirectory=utilitiespackage",
        "coverage",
        "pytest",
        "pytest-cov",
        "pytest-mock",
        "pytest-click",
        "pytest-pylint",
        "pytest-httpserver",
        "black",
        "flake8",
        "radon",
        "sqlalchemy",
        "flask",
        "flask-login",
        "flask-security",
        "flask-sqlalchemy",
        "flask-caching",
        "sqlalchemy-utils",
        "psycopg2-binary",
        "pylibmc",
        "pymongo",
        "humbledb",
        "bcrypt",
        "requests",
        "cherrypy",
        "dotty_dict",
        "django-htmlmin",
        "texttable",
        "cli-passthrough",
        "pycontracts",
    ],
    entry_points="""
        [console_scripts]
        sdm=sdm.__main__:main
    """,
)
