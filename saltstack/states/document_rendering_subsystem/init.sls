create_tech_docs_dir:
  cmd.run:
    - name: >
        mkdir -p /persistentworkingdirs/techdocs;

set_tech_docs_dir_perms:
  cmd.run:
    - name: >
        chmod -R 777 /persistentworkingdirs/techdocs;

/persistentworkingdirs/techdocs:
  file.recurse:
    - source: salt://document_rendering_subsystem/files/sample_tech_docs
    - user: vagrant
    - group: vagrant

create_tech_themes_dir:
  cmd.run:
    - name: >
        mkdir -p /persistentworkingdirs/themes;

set_tech_themes_dir_perms:
  cmd.run:
    - name: >
        chmod -R 777 /persistentworkingdirs/themes;

/persistentworkingdirs/themes/astropy-sphinx-theme:
  file.recurse:
    - source: salt://document_rendering_subsystem/files/themes/astropy-sphinx-theme
    - user: vagrant
    - group: vagrant

install_theme:
  cmd.run:
    - name: >
        unset SUDO_UID SUDO_GID;
        export PATH="/home/vagrant/dpe/sdm/miniconda3/bin:$PATH";
        source /home/vagrant/dpe/sdm/miniconda3/etc/profile.d/conda.sh;
        conda activate sdm;
        source /vagrant/env.sh;
        pip install .;
    - cwd: /vagrant/files/themes/astropy-sphinx-theme
    - runas: vagrant
