create_persistent_working_dirs:
  cmd.run:
    - name: >
        mkdir -p /persistentworkingdirs;

create_uploads_dir:
  cmd.run:
    - name: >
        mkdir -p /persistentworkingdirs/uploads;

create_files_dir:
  cmd.run:
    - name: >
        mkdir -p /persistentworkingdirs/files;

set_persistent_working_dirs_perms:
  cmd.run:
    - name: >
        chmod -R 777 /persistentworkingdirs;
