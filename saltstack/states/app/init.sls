set_app_perms:
  cmd.run:
    - name: >
        unset SUDO_UID SUDO_GID;
        chown -R vagrant /home/vagrant/.cache/pip;
        chown -R vagrant /home/vagrant/dpe;

install_app:
  cmd.run:
    - name: >
        unset SUDO_UID SUDO_GID;
        export LANG=en_US.UTF-8;
        export LANGUAGE=en_US:en;
        export LC_ALL=en_US.UTF-8;
        export PATH="/home/vagrant/dpe/sdm/miniconda3/bin:$PATH";
        source /home/vagrant/dpe/sdm/miniconda3/etc/profile.d/conda.sh;
        source env.sh;
        conda activate sdm;
        pip install -e .;
    - cwd: /vagrant
    - runas: vagrant
