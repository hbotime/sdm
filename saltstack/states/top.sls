base:
  '*':
    - apt
    - app
    - app.stockdirs
    - pdf_engine
    - document_rendering_subsystem
    - postgresql
    - mongo
    - memcached
    - nginx
    - supervisord
    - finalize
